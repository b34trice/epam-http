﻿using System;
using System.Threading.Tasks;
using Task1.Data;
using Task1.Interfaces;
using Unity;

namespace Task1
{
	class Program
	{
		private static IUnityContainer _container;
		private static string _url;
		private static Depth _depth;
		private static string _resultFolder;

		static Program()
		{
			_container = new UnityContainer();
		}

		/// <summary>
		/// 1 - Url
		/// 2 - Extentions
		/// 3 - Result folder
		/// </summary>
		/// <param name="args"></param>
		static async Task Main(string[] args)
		{
			InitParameters(args);
			InitDependencies();
			await _container.Resolve<ISiteDownloaderService>().Start(_depth, _resultFolder, _url);
			Console.WriteLine("End of process");
			Console.ReadKey();
		}

		private static void InitParameters(string[] args)
		{
			if (args.Length < 3)
			{
				throw new ArgumentException("Invalid parameters");
			}

			try
			{
				_depth = int.Parse(args[2]).GetDepth();
			}
			catch (Exception)
			{
				throw new ArgumentException("Depth must be int number");
			}

			_url = args[0];
			_resultFolder = args[1];
		}

		private static void InitDependencies()
		{
			_container.RegisterSingleton<IHtmlAnalizer, HtmlAnalizer>();
			_container.RegisterSingleton<IHtmlPageDownloader, HtmlPageDownloader>();
			_container.RegisterSingleton<IPageSaverService, PageSaverService>();
			_container.RegisterSingleton<ISiteDownloaderService, SiteDownloaderService>();
		}
	}
}