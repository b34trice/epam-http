﻿using System;
using System.IO;
using Task1.Interfaces;

namespace Task1
{
	public class PageSaverService : IPageSaverService
	{
		public void Save(string sourceCode, string fileName, string path)
		{
			try
			{
				Directory.CreateDirectory(path);
				File.WriteAllText($"{path}\\({Directory.GetFiles(path).Length}){fileName}", sourceCode);
				Console.WriteLine($"Save page as .html. Path: {path}");
			}
			catch (Exception e)
			{
				Console.WriteLine($"Error durring save page.Error message: {e.Message}");
			}
		}
	}
}