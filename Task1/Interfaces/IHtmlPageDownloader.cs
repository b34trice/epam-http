﻿using System.Threading.Tasks;

namespace Task1.Interfaces
{
	public interface IHtmlPageDownloader
	{
		public Task<string> GetHtmlAsString(string url);
	}
}
