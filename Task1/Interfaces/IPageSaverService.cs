﻿namespace Task1.Interfaces
{
	public interface IPageSaverService
	{
		void Save(string sourceCode, string fileName, string path);
	}
}
