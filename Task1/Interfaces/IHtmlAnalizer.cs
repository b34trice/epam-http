﻿using System.Collections.Generic;

namespace Task1.Interfaces
{
	public interface IHtmlAnalizer
	{
		IEnumerable<string> GetLinks(string page);
	}
}