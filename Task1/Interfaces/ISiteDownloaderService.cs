﻿using System.Threading.Tasks;
using Task1.Data;

namespace Task1.Interfaces
{
	public interface ISiteDownloaderService
	{
		Task Start(Depth depth, string resultFolder, string url);
	}
}