﻿using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Task1.Interfaces;

namespace Task1
{
	public class HtmlPageDownloader : IHtmlPageDownloader
	{
		private readonly HttpClient _client;

		public HtmlPageDownloader()
		{
			_client = new HttpClient();
		}

		public async Task<string> GetHtmlAsString(string url)
		{
			Console.WriteLine("Start download page. Url: " + url);
			var response = await _client.GetAsync(url);
			var content = await response.Content.ReadAsStreamAsync();
			string result;
			using (var sr = new StreamReader(content, Encoding.GetEncoding("iso-8859-1")))
			{
				result = sr.ReadToEnd();
			}
			Console.WriteLine("Downloading is finish");
			return result;
		}
	}
}