﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Task1.Data;
using Task1.Interfaces;
using System.Linq;
using System;

namespace Task1
{
	public class SiteDownloaderService : ISiteDownloaderService
	{
		private readonly IHtmlPageDownloader _downloader;
		private readonly IPageSaverService _saver;
		private readonly IHtmlAnalizer _analizer;

		private Depth _depth;
		private string _resultFolder;
		private List<string> _processedPages;
		private string _currentDomain;

		public SiteDownloaderService(IHtmlPageDownloader downloader, IPageSaverService saverService, IHtmlAnalizer analizer)
		{
			_processedPages = new List<string>();
			_downloader = downloader;
			_saver = saverService;
			_analizer = analizer;
		}

		public async Task Start(
			Depth depth,
			string resultFolder, string url)
		{
			_depth = depth;
			_currentDomain = GetDomain(url);
			_resultFolder = resultFolder;
			await Download(url);
		}

		private async Task Download(string url)
		{
			Console.WriteLine($"Currend url: {url}");
			_processedPages.Add(url);
			var currentPage = await _downloader.GetHtmlAsString(url);
			var pages = ValidatePages(_analizer.GetLinks(currentPage));
			Console.WriteLine($"Valid pages: {pages.Count()}");
			foreach (var page in pages)
			{
				var domain = GetDomain(page);
				Console.WriteLine($"Page domain: {domain}");
				_saver.Save(currentPage, $"{domain}.html", $"{_resultFolder}\\{domain}");

				if ((_depth == Depth.Local && domain == _currentDomain) || _depth == Depth.All)
				{
					Console.WriteLine("Start processing to new page");
					await Download(page);
				}
				else
				{
					Console.WriteLine("Page no valid by depth policy");
				}
			}
		}

		private IEnumerable<string> ValidatePages(IEnumerable<string> pages)
		{
			return pages.Where(page => page != null && !_processedPages.Contains(page) && page.StartsWith("https://"));
		}

		private string GetDomain(string url)
		{
			return url.Split("//")[1].Split('/')[0];
		}
	}
}