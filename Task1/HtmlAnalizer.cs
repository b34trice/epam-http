﻿using CsQuery;
using System.Collections.Generic;
using Task1.Interfaces;
using System.Linq;
using System;

namespace Task1
{
	public class HtmlAnalizer : IHtmlAnalizer
	{
		public IEnumerable<string> GetLinks(string page)
		{
			var result = new List<string>();
			var cq = CQ.Create(page);
			result.AddRange(cq.Find("a").Select(obj => obj.GetAttribute("href")));
			Console.WriteLine($"Find {result.Count} children pages");
			return result;
		}
	}
}