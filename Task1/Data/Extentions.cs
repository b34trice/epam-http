﻿using System;

namespace Task1.Data
{
	public static class Extentions
	{
		public static Depth GetDepth(this int level)
		{
			switch (level)
			{
				case 0: return Depth.Local;
				case 1: return Depth.All;
				default: throw new ArgumentException("Invalid depth level");
			}
		}
	}
}
