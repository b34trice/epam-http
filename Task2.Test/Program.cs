﻿using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Task2.Test
{
	class Program
	{
		static async Task Main(string[] args)
		{
			var httpClient = new HttpClient();
			var response = await httpClient.GetAsync("http://localhost:5000/northwind");
			var result = await response.Content.ReadAsByteArrayAsync();
			File.WriteAllBytes("C://temp//Result.csv", result);

			httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/xml"));
			var bytes = await response.Content.ReadAsByteArrayAsync();
			File.WriteAllBytes("C://temp/Result.xml", bytes);
		}
	}
}