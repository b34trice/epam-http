using Moq;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Task1;
using Task1.Data;
using Task1.Interfaces;
using Xunit;

namespace Task1UnitTests
{
	public class Task1UnitTests
	{
		private readonly IHtmlAnalizer _htmlAnalizer;
		private readonly IHtmlPageDownloader _downloader;
		private readonly ISiteDownloaderService _siteDownloaderService;

		private Mock<IHtmlAnalizer> _mockAnalizer;
		private Mock<IPageSaverService> _mockPageSaver;
		private Mock<IHtmlPageDownloader> _mockHtmlDownloader;

		public Task1UnitTests()
		{
			_htmlAnalizer = new HtmlAnalizer();
			_downloader = new HtmlPageDownloader();

			_mockAnalizer = new Mock<IHtmlAnalizer>();
			_mockPageSaver = new Mock<IPageSaverService>();
			_mockHtmlDownloader = new Mock<IHtmlPageDownloader>();

			_mockAnalizer.Setup(m => m.GetLinks(It.IsAny<string>())).Returns(new List<string> { "", "" });
			_mockHtmlDownloader.Setup(m => m.GetHtmlAsString(It.IsAny<string>())).Returns(Task.FromResult(string.Empty));
			_siteDownloaderService = new SiteDownloaderService(_mockHtmlDownloader.Object, _mockPageSaver.Object, _mockAnalizer.Object);
		}

		[Theory]
		[InlineData("https://www.google.com/")]
		public void HtmlDownloaderUnitTest_GetHtml(string url)
		{
			var expectedResult = File.ReadAllText("googleHtml.txt");
			var actualResult = _downloader.GetHtmlAsString(url).Result;
			Assert.Equal(expectedResult, actualResult);
		}

		[Theory]
		[InlineData("https://www.google.com/", 20)]
		public void HtmlAnalizerUnitTest_GetPages(string url, int pageCounter)
		{
			var page = _downloader.GetHtmlAsString(url).Result;
			var actualResult = _htmlAnalizer.GetLinks(page);
			Assert.Equal(pageCounter, actualResult.Count());
		}

		[Theory]
		[InlineData(Depth.All, "C:\\temp", "https://google.com")]
		public async Task SiteDownloaderServiceUnitTest_CheckMocks(Depth depth, string resultFolder, string url)
		{
			await _siteDownloaderService.Start(depth, resultFolder, url);
			_mockHtmlDownloader.Verify(m => m.GetHtmlAsString(url), Times.Once);
			_mockAnalizer.Verify(m => m.GetLinks(It.IsAny<string>()), Times.Once);
			_mockPageSaver.Verify(m => m.Save(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
		}
	}
}