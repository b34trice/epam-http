﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using System.IO;
using Task2.Interfaces;

namespace Task2.Controllers
{
	[ApiController]
	[Route("[controller]")]
	public class NorthwindController : ControllerBase
	{
		private const string EXCEL_HEADER = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
		private const string XML_HEADER = "text/xml";

		private readonly string _connectionString;

		public NorthwindController(IConfiguration configuration)
		{
			_connectionString = configuration.GetConnectionString("MSSQL");
		}

		[HttpGet]
		public FileResult Get(string customer = null, string dateFrom = null, string take = null)
		{
			var query = new QueryGenerator().GetQuery(customer, dateFrom, take);
			var orders = new DatabaseService().GetOrders(_connectionString, query);

			IHandleStrategy handleStrategy;
			StringValues header = string.Empty;
			StringValues responseHeader = string.Empty;
			string fileExtention;
			Request.Headers.TryGetValue("Accept", out header);

			if (header == XML_HEADER)
			{
				handleStrategy = new HandlerXmlFile();
				responseHeader = XML_HEADER;
				fileExtention = ".xml";
			}
			else
			{
				handleStrategy = new HandlerExcelFile();
				responseHeader = EXCEL_HEADER;
				fileExtention = ".csv";
			}

			Response.Headers.Add("Content-Type", responseHeader);
			return File(handleStrategy.GetBytes(orders), EXCEL_HEADER, $"Result{fileExtention}");
		}
	}
}
