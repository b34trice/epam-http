﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Task2.Data;
using Task2.Interfaces;

namespace Task2
{
    public class HandlerXmlFile : IHandleStrategy
    {
        public byte[] GetBytes(IEnumerable<Order> orders)
        {
            var xml = new XElement("Orders",
                from order in orders
                select new XElement("Order",
                             new XAttribute("OrderId", order.OrderId),
                             new XAttribute("CustomerId", order.CustomerId == null ? string.Empty : order.CustomerId),
                             new XAttribute("EmployeeId", order.EmployeeId == null ? 0 : order.EmployeeId),
                             new XAttribute("Freight", order.Freight == null ? 0 : order.Freight),
                             new XAttribute("OrderDate", order.OrderDate == null ? DateTime.MinValue : order.OrderDate),
                             new XAttribute("RequiredDate", order.RequiredDate == null ? DateTime.MinValue : order.RequiredDate),
                             new XAttribute("ShipAddress", order.ShipAddress == null ? string.Empty : order.ShipAddress),
                             new XAttribute("ShipCity", order.ShipCity == null ? string.Empty : order.ShipCity),
                             new XAttribute("ShipCountry", order.ShipCountry == null ? string.Empty : order.ShipCountry),
                             new XAttribute("ShipName", order.ShipName == null ? string.Empty : order.ShipName),
                             new XAttribute("ShippedDate", order.ShippedDate == null ? DateTime.MinValue : order.ShippedDate),
                             new XAttribute("ShipPostalCode", order.ShipPostalCode == null ? string.Empty : order.ShipPostalCode),
                             new XAttribute("ShipRegion", order.ShipRegion == null ? string.Empty : order.ShipRegion),
                             new XAttribute("ShipVia", order.ShipVia == null ? 0 : order.ShipVia)));

            using (var memoryStream = new MemoryStream())
            {
                xml.Save(memoryStream);
                return memoryStream.ToArray();
            }
        }
    }
}
