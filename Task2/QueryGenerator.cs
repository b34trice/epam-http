﻿namespace Task2
{
	public class QueryGenerator
	{
		public string GetQuery(string customer, string dateFrom, string take)
		{
			string takeResut = take == null ? "*" : take;
			string customerResult = customer == null ? ">=0" : $"={customer}";
			string dateResult = dateFrom == null ? string.Empty : dateFrom;
			string queryParams = $"WHERE (OrderID {customerResult} AND OrderDate > '{dateResult}')";
			return $"SELECT {takeResut} FROM Orders {queryParams}";
		}
	}
}
