﻿using ExcelLibrary.SpreadSheet;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Task2.Data;
using Task2.Interfaces;

namespace Task2
{
	public class HandlerExcelFile : IHandleStrategy
	{
		public byte[] GetBytes(IEnumerable<Order> orders)
		{
			var workbook = new Workbook();
			var worksheet = new Worksheet("Sheet");
			for (int i = 0; i < orders.Count(); i++)
			{
				worksheet.Cells[i, 0] = new Cell(orders.ElementAt(i).CustomerId);
				worksheet.Cells[i, 1] = new Cell(orders.ElementAt(i).EmployeeId);
				worksheet.Cells[i, 2] = new Cell(orders.ElementAt(i).Freight);
				worksheet.Cells[i, 3] = new Cell(orders.ElementAt(i).OrderDate);
				worksheet.Cells[i, 4] = new Cell(orders.ElementAt(i).OrderId);
				worksheet.Cells[i, 5] = new Cell(orders.ElementAt(i).RequiredDate);
				worksheet.Cells[i, 6] = new Cell(orders.ElementAt(i).ShipAddress);
				worksheet.Cells[i, 7] = new Cell(orders.ElementAt(i).ShipCity);
				worksheet.Cells[i, 8] = new Cell(orders.ElementAt(i).ShipCountry);
				worksheet.Cells[i, 9] = new Cell(orders.ElementAt(i).ShipName);
				worksheet.Cells[i, 10] = new Cell(orders.ElementAt(i).ShippedDate);
				worksheet.Cells[i, 11] = new Cell(orders.ElementAt(i).ShipPostalCode);
				worksheet.Cells[i, 12] = new Cell(orders.ElementAt(i).ShipRegion);
				worksheet.Cells[i, 13] = new Cell(orders.ElementAt(i).ShipVia);
			}
			workbook.Worksheets.Add(worksheet);
			using (var stream = new MemoryStream())
			{
				workbook.SaveToStream(stream);
				return stream.ToArray();
			}
		}
	}
}
