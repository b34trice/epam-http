﻿using System.Collections.Generic;
using Task2.Data;
using Task2.Interfaces;
using Dapper;
using System.Data.SqlClient;

namespace Task2
{
	public class DatabaseService : IDatabaseService
	{
		public IEnumerable<Order> GetOrders(string connectionString, string query)
		{
			IEnumerable<Order> users;
			using (var db = new SqlConnection(connectionString))
			{
				users = db.Query<Order>(query);
			}
			return users;
		}
	}
}
