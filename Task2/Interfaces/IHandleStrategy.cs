﻿using System.Collections.Generic;
using Task2.Data;

namespace Task2.Interfaces
{
	public interface IHandleStrategy
	{
		byte[] GetBytes(IEnumerable<Order> orders);
	}
}
