﻿using System.Collections.Generic;
using Task2.Data;

namespace Task2.Interfaces
{
	public interface IDatabaseService
	{
		IEnumerable<Order> GetOrders(string connectionString, string query);
	}
}
